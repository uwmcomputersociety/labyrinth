```
88888888888888888888888888888888888888888888888888888888888888888888888
88.._|      | `-.  | `.  -_-_ _-_  _-  _- -_ -  .'|   |.'|     |  _..88
88   `-.._  |    |`!  |`.  -_ -__ -_ _- _-_-  .'  |.;'   |   _.!-'|  88
88      | `-!._  |  `;!  ;. _______________ ,'| .-' |   _!.i'     |  88
88..__  |     |`-!._ | `.| |I_____________I||."'|  _!.;'   |     _|..88
88   |``"..__ |    |`";.|  | |     _     | |'| _!-|   |   _|..-|'    88
88   |      |``--..|_ | `; |E|    / \    |E|.'|   |_..!-'|     |     88
88   |      |    |   |`-,!_| | /-<THE>-\ | ||.!-;'  |    |     |     88
88___|______|____!.,.!,.!,!|E| LABYRINTH |E|,!,.!.,.!..__|_____|_____88
88      |     |    |  |  | | |  /_/ \_\  | || |   |   |    |      |  88
88      |     |    |..!-;'`|E|         , |E| |`-..|   |    |      |  88
88      |    _!.-j'  | _!,"| |         ] | ||!._|  `i-!.._ |      |  88
88     _!.-'|    | _."|  !;|||           |||`.| `-._|    |``-.._  |  88
88..-i'     |  _.''|  !-| !| |           | |.|`-. | ``._ |     |``"..88
88   |      |.|    |.|  !| |C|           |C||`. |`!   | `".    |     88
88   |  _.-'  |  .'  |.' |/| |           | |! |`!  `,.|    |-._|     88
88  _!"'|     !.'|  .'| .'|[S]___________[S] \|  `. | `._  |   `-._  88
88-'    |   .'   |.|  |/| /       `         \|`.  |`!    |.|      |`-88
88      |_.'|   .' | .' |/`  ,.          ,   \  \ |  `.  | `._    |  88
88     .'   | .'   |/|  /   `      '   ,     '\ |`!   |`.|    `.  |  88
88  _.'     !'|   .' | /   .   ,               \|  `  |  `.    |`.|  88
88888888888888888888888888888888888888888888888888888888888888888888888
```
This directory contains a series of challenges.
These challenges may be completed to receive various perks in IEEE-CS.
Ask a club officer for details on the rewards.

The format is as follows:
	Each directory has a challenge which can be seen by reading the "README".
	Completing each challenge will result in a two digit number.
	These numbers should be privately messaged to any of the club officers
	who will grant you rewards if they are correct.

All of these challenges are possible with bash commands or writing scripts.
It may take some googling but you should be able to do it -- All have been tested to be possible.
It's fair game to copy these files or clone the repo to complete it, but do not tell anyone else
the flags or how you arrived to these answers or we will find out. 

### Hints:
1. Many of these can be brute-forced, but try to use iteration to complete puzzles.
   It will save you valuable time, and after all you only have so many minutes left in your life.
   Use them wisely and learn something new while you're at it!
2. Use google.
3. Running 'python3' will open a python interpreter in the current directory.
   Check out how to load a text file into this interpreter and manipulate it.
   This will make your life easier.
4. Many of these challenges are trying to get you familiar with bash tools,
   or linux utilities, or programmatic thinking. Don't be afraid to write some code.

*Good luck!*
