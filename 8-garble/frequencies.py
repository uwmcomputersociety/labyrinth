#!/usr/bin/env python

# You may find this script useful...

import urllib2
from functools import reduce
BASE_URL = "https://www.gutenberg.org/files"

# More sample texts => More accuracy
def analyze(count):
  count = min(10,count)
  table = [0.0] * 128

  # Read and tally chars in all sample texts
  for i in range(2,count+1):
    url = "{0}/{1}/{1}.txt".format(BASE_URL, i)
    print "Receiving {0}...".format(url)
    text = urllib2.urlopen(url).read()
    for ch in text:
      table[ord(ch)] += 1
  
  # Count total chars and calculate frequencies for each char
  print "Analyzing..."
  total_chars = reduce((lambda x, y: x + y), table)
  result = {}
  for index,count in enumerate(table):
    result[chr(index)] = count / total_chars * 100
  return result
